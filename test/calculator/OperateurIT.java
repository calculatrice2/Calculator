package calculator;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class OperateurIT {
    
    public OperateurIT() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of Additionner method, of class Operateur.
     */
    @Test
    public void testAdditionner() {
        System.out.println("Additionner");
        int a = 5;
        int b = 3;
        Operateur instance = new Operateur();
        int expResult = 8;
        int result = instance.Additionner(a, b);
        assertEquals(expResult, result);
    }

    /**
     * Test of Soustraire method, of class Operateur.
     */
    @Test
    public void testSoustraire() {
        System.out.println("Soustraire");
        int a = 10;
        int b = 5;
        Operateur instance = new Operateur();
        int expResult = 5;
        int result = instance.Soustraire(a, b);
        assertEquals(expResult, result);
    }

    /**
     * Test of Multiplier method, of class Operateur.
     */
    @Test
    public void testMultiplier() {
        System.out.println("Multiplier");
        int a = 2;
        int b = 8;
        Operateur instance = new Operateur();
        int expResult = 16;
        int result = instance.Multiplier(a, b);
        assertEquals(expResult, result);
    }

    /**
     * Test of Diviser method, of class Operateur.
     */
    @Test
    public void testDiviser() {
        System.out.println("Diviser");
        int a = 20;
        int b = 4;
        Operateur instance = new Operateur();
        int expResult = 5;
        int result = instance.Diviser(a, b);
        assertEquals(expResult, result);
    }

    /**
     * Test of Calculmoyenne method, of class Operateur.
     */
    @Test
    public void testCalculmoyenne() {
        System.out.println("Calcul moyenne");
        double[] tableau = {50, 50};
        Operateur instance = new Operateur();
        double expResult = 50;
        double result = instance.Calculmoyenne(tableau);
        assertEquals(expResult, result, 0.0);
    }
}