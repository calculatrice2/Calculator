/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator;

import java.util.Scanner;

/**
 *
 * @author Lucia
 */
public class Calculator {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Calculatrice");
        System.out.println("1. Addition");
        System.out.println("2. Soustraction");
        System.out.println("3. Multiplication");
        System.out.println("4. Division");
        System.out.println("5. Moyenne d'un tableau");
        System.out.println("-------------------------------");
        System.out.print("Quel calcul voulez-vous faire : ");
        Scanner scan = new Scanner(System.in);
        int choix = scan.nextInt();
        
        switch(choix){
            case 1:
                System.out.print("Entrer un nombre: ");
                int a = scan.nextInt();
                System.out.print("Entrer un deuxième nombre: ");
                int b = scan.nextInt();
                Operateur addition = new Operateur();
                System.out.println("Le résultat de l'addition est : "+addition.Additionner(a, b));
                break;
                
            case 2:
                System.out.print("Entrer un nombre: ");
                a = scan.nextInt();
                System.out.print("Entrer un deuxieme nombre: ");
                b = scan.nextInt();
                Operateur soustraction = new Operateur();
                System.out.println("Le résultat de la Soustraction est: "+soustraction.Soustraire (a, b));
                break;
                
            case 3:
                System.out.print("Entrer un nombre: ");
                a = scan.nextInt();
                System.out.print("Entrer un deuxieme nombre: ");
                b = scan.nextInt();
                Operateur multiplication = new Operateur();
                System.out.println("Le résultat de la multiplication est : "+multiplication.Multiplier(a,b));
                break;
                
            case 4:
                System.out.print("Entrer un nombre: ");
                a = scan.nextInt();
                System.out.print("Entrer un deuxieme nombre: ");
                b = scan.nextInt();
                Operateur division = new Operateur();
                System.out.println("Le résultat de la Division est: "+division.Diviser(a,b));
                break;
                
            case 5:
                System.out.print("Donner le nombre d'élément de votre tableau : ");
                int longueur = scan.nextInt();
                double[] tableau = new double[longueur];
                for(int i=0; i<longueur; i++){
                    System.out.print("Nombre "+ i + 1);
                    tableau[i] = scan.nextInt();
                }
                Operateur moyenne = new Operateur();
                System.out.println("La moyenne de votre tableau est  : "+moyenne.Calculmoyenne(tableau));
                break;
                
            default:
                System.out.println("Choix invalide");
            }
        }
    } 

