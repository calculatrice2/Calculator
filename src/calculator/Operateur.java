/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Lucia
 */
public class Operateur {
        public int Additionner(int a, int b) {
        return a + b;
    }

    //méthode de soustraction
    public int Soustraire(int a, int b) {
        return a - b;
    }
    
   
    public int Multiplier(int a, int b) {
        return a * b;
    }

    //méthode de soustraction
    public int Diviser(int a, int b) {
        if (b != 0) {
            return a / b;
        } else {
            throw new ArithmeticException("Attention: On ne peut diviser un nombre par zéro");
        }
       
    }
    
    
    public double Calculmoyenne(double[] tableau){
        if (tableau != null){

            int somme = 0;

        for (double nombre : tableau){
            somme += nombre;
        }

        return Diviser(somme, tableau.length);

        } else {
            throw new ArithmeticException("Erreur!!");
        }
    
    }     
}


